package com.trinoor

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeDriverService
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities

/* TODO:
 * 	- [ ] cleanup
 *  - [ ] add better comments
 */

public class Electron {
	/**
	 * Launch an electron application as an executable
	 *
	 * @param electronExecutablePath  path to the electron executable
	 * @param userDataDir             path to a data directory
	 * @param debugPort               port to use for remote debugging
	 * @param useNextAvailablePort    if `debugPort` is unavailable, use the next free port
	 * @param arguments               list of command line flags to pass to electron
	 * @param environment             list of environment variables to set
	 * @return  an ElectronProcess instance for the launched application
	 */
	@Keyword
	public ElectronProcess launchElectronExecutable(String electronExecutablePath,
			String userDataDir = null, int debugPort = 9222,
			boolean useNextAvailablePort = true, Map<String, String> arguments = [:],
			Map<String, String> environment = [:]) {
		KeywordUtil.logInfo("""
			new ElectronProcess(
				executablePath: $electronExecutablePath
				dataDir:        $userDataDir
				port:           $debugPort
				nextAvailable:  $useNextAvailablePort
				args:           $arguments
				env:            $environment
			)
		""")
		// Start up the electron application with the provided settings
		ElectronProcess ep = new ElectronProcess(electronExecutablePath,
				debugPort, userDataDir, useNextAvailablePort, arguments, environment)

		// Try to launch the electron application
		try {
			ep.start()
		} catch (InvalidPortException e) {
			KeywordUtil.logInfo("Invalid port used when launching client: '$debugPort'")
			throw e
		} catch (DataDirBusyException e) {
			KeywordUtil.logInfo("Specified data directory is in use: '$userDataDir'")
			throw e
		}

		// Determine if it successfully launched
		if (ep.isAlive()) {
			KeywordUtil.logInfo("Successfully launched '$electronExecutablePath'")
			return ep
		} else {
			KeywordUtil.logInfo("Failed to launch '$electronExecutablePath'")
			return null
		}
	}

	/**
	 * Connect to a chrome client with --remote-debugging-port enabled
	 *
	 * @param chromeDriverPath  path to the chromedriver executable
	 * @param host              host the chrome client is running on
	 * @param port              the chrome client's remote debugging port
	 * @return  a WebDriver connected to the chrome client
	 */
	@Keyword
	public WebDriver connectToRunningClient(String chromeDriverPath, int port = 9222, String host = 'localhost') {
		// Set up custom chromedriver
		System.setProperty("webdriver.chrome.driver", chromeDriverPath)

		// Set the host and port of the running
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("debuggerAddress", "$host:$port");

		// Create chromedriver
		WebDriver driver = new ChromeDriver(options);

		// Change Katalon's default chromedriver to this one
		DriverFactory.changeWebDriver(driver)

		return driver
	}

	/**
	 * Launch an electron executable and connect to it with chromedriver.
	 *
	 * @param electronExecutablePath  path to the electron executable
	 * @param chromeDriverPath        path to the chromedriver executable
	 * @param userDataDir             path to a data directory for the electron client
	 * @return  an ElectronConnector instance with the driver and electron process
	 */
	@Keyword
	public ElectronConnector launchAndConnect(String electronExecutablePath,
			String chromeDriverPath, String userDataDir = null) {
		ElectronProcess ep = launchElectronExecutable(electronExecutablePath, userDataDir)
		if (ep) {
			WebDriver driver = connectToRunningClient(chromeDriverPath, "127.0.0.1", ep.debugPort)
			return new ElectronConnector(driver, ep)
		}
		return null
	}
}

/**
 * A class for launching an electron client with specific configuration
 * settings.
 */
class ElectronProcess {
	public Process process
	public String executable
	public Map<String, String> arguments
	public Map<String, String> environment
	public int debugPort
	public boolean useNextAvailablePort
	public String userDataDir
	public BufferedReader stdout
	public BufferedReader stderr

	public ElectronProcess(String executable,
	int debugPort = 9222, String userDataDir = null,
	boolean useNextAvailablePort = true, Map<String, String> arguments = [:], Map<String, String> environment = [:]) {
		this.executable = executable;
		this.userDataDir = userDataDir;
		this.arguments = arguments;
		this.environment = environment;
		this.debugPort = debugPort;
		this.useNextAvailablePort = useNextAvailablePort;
		this.stdout = null;
		this.stderr = null;
		this.process = null;
	}

	/**
	 * Convert args from map to a list in the format ['--key="value"', ...],
	 * where the values are properly escaped. Also adds the debug port and 
	 * user data directory if specified
	 */
	private List<String> setupArgs() {
		// Find first valid port
		if (this.useNextAvailablePort) {
			this.debugPort = findAvailablePort();
		}
		// Add the debug port
		this.arguments["remote-debugging-port"] = this.debugPort.toString();

		// Add the user data dir if specified
		if (this.userDataDir != null) {
			this.arguments["user-data-dir"] = this.userDataDir;
		}

		List<String> args = new ArrayList<String>();
		for (Map.Entry<String, String> entry : this.arguments.entrySet()) {
			// escape any double quotes in the value
			String value = entry.getValue().replaceAll("\"", "\\\"");
			args.add("--" + entry.getKey() + "=\"" + value + "\"");
		}
		return args;
	}

	/**
	 * Launch the executable and determine if .
	 */
	public void start() {
		List<String> args = setupArgs();
		args = [this.executable]+ args;
		KeywordUtil.logInfo("Launching: '" + args.join(" ")) + "'"
		ProcessBuilder pb = new ProcessBuilder();
		pb.command(args);
		pb.environment().putAll(this.environment);
		this.process = pb.start();

		this.stdout = new BufferedReader(new InputStreamReader(this.process.getInputStream()));
		this.stderr = new BufferedReader(new InputStreamReader(this.process.getErrorStream()));

		/* Wait for the first line of stderr.
		 * - If stderr contains "DevTools listening on", then the process is
		 *   ready to accept connections.
		 * - If stderr contains "Cannot start http server", then the process
		 *   failed to start.
		 */
		String line = this.stderr.readLine();
		while (true) {
			if (line.contains("DevTools listening on")) {
				KeywordUtil.logInfo("Listening on port " + this.debugPort)
				break
			} else if (line.contains("Cannot start http server")) {
				throw new InvalidPortException("Cannot start http server on port " + this.debugPort);
			}
			line = this.stderr.readLine();
		}

		/* Wait 3 seconds to make sure the process hasn't exited due to the
		 * user data directory being in use */
		sleep(3000);
		if (!this.process.isAlive()) {
			File userDataDirectory = new File(this.userDataDir);
			throw new DataDirBusyException("User Data Directory '" + userDataDirectory.getAbsolutePath() + "' is in use");
		}
	}

	/**
	 * Check if the electron application is still running
	 * @return  true if the electron application is still running, else false
	 */
	public boolean isAlive() {
		return this.process != null && this.process.isAlive();
	}

	/**
	 * Wait for the next line of the process' stdout
	 * @return  the next line of stdout
	 */
	public String readLineOut() {
		return this.stdout.readLine();
	}

	/**
	 * Wait for the next line of the process' stderr
	 * @return  the next line of stderr
	 */
	public String readLineErr() {
		return this.stderr.readLine();
	}

	/**
	 * Starting with the specified port, incrementally search until an open port
	 * is found and return it
	 * @param port  the port at which to start checking
	 * @return  the first open port found
	 */
	private int findAvailablePort(int port = this.debugPort) {
		while (true) {
			try {
				ServerSocket socket = new ServerSocket(port);
				socket.close();
				return port;
			} catch (IOException e) {
				port++;
			}
		}
	}
}

class InvalidPortException extends Exception {
	InvalidPortException() {
		super("Invalid port")
	}
	InvalidPortException(String message) {
		super(message)
	}
}

class DataDirBusyException extends Exception {
	DataDirBusyException() {
		super("Data directory is busy")
	}
	DataDirBusyException(String message) {
		super(message)
	}
}

/**
 * Container for an ElectronProcess and a chromedriver connected to it
 */
class ElectronConnector {
	public WebDriver driver
	public ElectronProcess process

	public ElectronConnector(WebDriver driver, ElectronProcess process) {
		this.driver = driver
		this.process = process
	}
}

