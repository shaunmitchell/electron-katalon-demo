package com.trinoor

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class TAShelix {
	/**
	 * Navigates TAShelix modules given an ordered hierarchy of items to click
	 * through.
	 *
	 * @param modules  the ordered list of modules to click
	 */
	@Keyword
	public void navigateModules(List<String> modules) {
		for (String moduleName in modules) {
			KeywordUtil.logInfo("clicking module: '$moduleName'")
			TestObject to = findTestObject('Common/po_Module_by_Name', [name: moduleName])

			// Wait for the module to be present
			WebUI.waitForElementPresent(to, 3)

			// Check to see if it's visible
			if (!WebUI.verifyElementInViewport(to, 1, FailureHandling.OPTIONAL)) {
				// If it's not, scroll to it
				WebUI.scrollToElement(to, 2)
			}

			// Click it
			WebUI.click(to)
		}
	}
}
