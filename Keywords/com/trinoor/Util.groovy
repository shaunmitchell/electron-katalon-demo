package com.trinoor

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import java.time.LocalDateTime
import java.util.regex.Pattern

public class Util {
	/**
	 * Returns a string representing the current date using the format specified
	 * and offset by the number of days specified. See the DateTimeFormatter
	 * docs for information on the formatting specifications:
	 * https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
	 * 
	 * Example:
	 *   ```
	 *   // Get tomorrow's date in the format 01/18/2022
	 *   generateDatetimeString("MM/dd/yyyy", +1)
	 *   ```
	 *
	 * @param format  a SimpleDate
	 */
	@Keyword
	public String generateDatetimeString(String format, int daysOffset) {
		def dt = LocalDateTime.now()
		return dt.plusDays(daysOffset).format(format)
	}

	/**
	 * Returns a string representing the current date using the format
	 * specified. See the DateTimeFormatter docs for information on the
	 * formatting specifications:
	 * https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
	 *
	 * Example:
	 *   ```
	 *   // Get tomorrow's date in the format 01/18/2022
	 *   generateDatetimeString("MM/dd/yyyy", +1)
	 *   ```
	 *
	 * @param format  a SimpleDate
	 */
	@Keyword
	public String generateDatetimeString(String format) {
		def dt = LocalDateTime.now()
		return dt.format(format)
	}

	/**
	 * Create a new Test Object from an xpath
	 *
	 * @param xpath
	 * @return
	 */
	@Keyword
	public TestObject createTestObjectByXpath(String xpath) {
		TestObject testObject = new TestObject()
		return testObject.addProperty('xpath', ConditionType.EQUALS, xpath, true)
	}

	/**
	 * Escapes single quotes using the `concat()` function in Xpath statements.
	 *
	 * @param text  the text to quote
	 * @return  a string with the escaped text using `concat(...)`
	 */
	private String xpathStringEscape(String text) {
		return "concat('" + text.split("'").join("', \"'\" , '") + "', '')"
	}

	/**
	 * Creates a Test Object that looks for an element (including input buttons
	 * and text fields) containing the specified text
	 *
	 * @param text  the text to search for in element
	 * @return  a Test Object referring to an element with the specified text
	 */
	private TestObject createTextObject(String text) {
		text = xpathStringEscape(text)
		String xpath = "//*[contains(text(), $text) or (local-name() = 'input' and (@type = 'submit' or @type = 'text') and contains(@value, $text))]"
		return createTestObjectByXpath(xpath)
	}

	/**
	 * Wait for an element containing the specified text to be present in the
	 * DOM
	 *
	 * @param text         the text to wait for
	 * @param timeOut      the maximum time to wait (seconds)
	 * @param flowControl  how to handle whether or not the text was found
	 * @return  boolean for whether or not the element was found on the page
	 */
	@Keyword
	public boolean waitForTextPresent(String text, int timeOut = RunConfiguration.getTimeOut(), FailureHandling flowControl = RunConfiguration.getDefaultFailureHandling()) {
		TestObject testObject = createTextObject(text)
		return WebUI.waitForElementPresent(testObject, timeOut, flowControl)
	}

	/**
	 * Wait for an element containing the specified text to be visible in the
	 * DOM
	 *
	 * @param text         the text to wait for
	 * @param timeOut      the maximum time to wait (seconds)
	 * @param flowControl  how to handle whether or not the text was found
	 * @return  boolean for whether or not the element was found on the page
	 */
	@Keyword
	public boolean waitForTextVisible(String text, int timeOut = RunConfiguration.getTimeOut(), FailureHandling flowControl = RunConfiguration.getDefaultFailureHandling()) {
		TestObject testObject = createTextObject(text)
		return WebUI.waitForElementVisible(testObject, timeOut, flowControl)
	}

	/**
	 * Waits for a Test Object's attribute to match a given regex and returns
	 * true if the attribute matches within the specified timeout, else returns
	 * false.
	 * 
	 * @param to             the test object whose attribute should be checked
	 * @param attribute      the attribute name to check
	 * @param pattern        a regex to match against the attribute value
	 * @param timeout        the seconds to wait before giving up
	 * @param sleepInterval  the ms between polling the element attribute
	 * @return  boolean representing if the attribute matched within the timeout
	 */
	@Keyword
	public boolean waitForAttributeMatches(TestObject to, String attribute, String pattern, int timeout = RunConfiguration.getTimeOut()) {
		Long startTime = new Date().getTime() / 1000
		while (!(WebUI.getAttribute(to, attribute) ==~ pattern)) {
			Long runDuration = (new Date().getTime() / 1000) - startTime
			if (runDuration > timeout) {
				KeywordUtil.markWarning("Attribute '$attribute' of object '$to' doesn't match '$pattern' after $timeout seconds")
				return false
			}
			sleep(500)
		}
		return true
	}

	/**
	 * Waits for a Test Object's attribute to equal the given value and returns
	 * true if the attribute matches within the specified timeout, else returns
	 * false.
	 * 
	 * @param to             the test object whose attribute should be checked
	 * @param attribute      the attribute name to check
	 * @param value          the value the attribute must equal
	 * @param timeout        the seconds to wait before giving up
	 * @param sleepInterval  the ms between polling the element attribute
	 * @return  boolean representing if the attribute matched within the timeout
	 */
	public boolean waitForAttributeEquals(TestObject to, String attribute, String value, int timeout = RunConfiguration.getTimeOut()) {
		Long startTime = new Date().getTime() / 1000
		while (!WebUI.getAttribute(to, attribute).equals(value)) {
			Long runDuration = (new Date().getTime() / 1000) - startTime
			if (runDuration > timeout) {
				KeywordUtil.markWarning("Attribute '$attribute' of object '$to' doesn't equal '$value' after $timeout seconds")
				return false
			}
			sleep(500)
		}
		return true
	}

	/**
	 * Waits for a Test Object's attribute have any value and returns true if
	 * the attribute is not empty within the specified timeout, else returns
	 * false.
	 * 
	 * @param to             the test object whose attribute should be checked
	 * @param attribute      the attribute name to check
	 * @param timeout        the seconds to wait before giving up
	 * @param sleepInterval  the ms between polling the element attribute
	 * @return  boolean representing if the attribute matched within the timeout
	 */
	@Keyword
	public boolean waitForAttributeNotEmpty(TestObject to, String attribute, int timeout = RunConfiguration.getTimeOut()) {
		return waitForAttributeMatches(to, attribute, /.+/, timeout)
	}
}
