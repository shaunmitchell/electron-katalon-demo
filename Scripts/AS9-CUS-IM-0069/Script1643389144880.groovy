import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Sign in to the TAShelix client'
WebUI.callTestCase(findTestCase('Common/LaunchAndLogin'), null, FailureHandling.STOP_ON_FAILURE)

// TODO: insert test data

// Set up variables to reduce code changes when adding test data
String catalogId = '0000000003'
String totalQty = '19994'
String qLevel = '0'

/** Step 1.1 **/

'Navigate to Warehouse Data Locations'
CustomKeywords.'com.trinoor.TAShelix.navigateModules'(['Asset Suite', 'Inventory', 'Finds', 'D252. Warehouse Location Data'])

'Click the Search button to look for an inventory item'
WebUI.click(findTestObject('Common/button_Search'))

'Set the Catalog ID'
WebUI.setText(findTestObject('Common/po_Element_by_Attribute', [attribute: 'placeholder', value: 'Catalog ID']), catalogId)

'Set the Q Level to 0'
WebUI.setText(findTestObject('Common/po_Element_by_Attribute', [attribute: 'placeholder', value: 'Q Level']), qLevel)

'Click the search button'
WebUI.click(findTestObject('Common/button_Search'))

'Wait for results to appear'
if (!WebUI.waitForElementPresent(findTestObject('Common/button_AddFirstSearchResult'), 5)) {
	KeywordUtil.markErrorAndStop("Could not find item with Catalog ID=$catalogId and Q Level=$qLevel")
}

'Click the button to add the first result'
WebUI.click(findTestObject('Common/button_AddFirstSearchResult'))

'Wait for the Total Qty field to appear'
WebUI.waitForElementPresent(findTestObject('Common/po_Input_by_Label', [label: 'Total Qty']), 5)

'Wait for the Total Qty field\'s value to update'
CustomKeywords.'com.trinoor.Util.waitForAttributeNotEmpty'(findTestObject('Common/po_Input_by_Label', [label: 'Total Qty']), 'value', 3)

'Get the value of the Total Qty field'
String value = WebUI.getAttribute(findTestObject('Common/po_Input_by_Label', [label: 'Total Qty']), 'value')

'Ensure it has the correct value'
assert value == totalQty : "Total Quantity of Catalog ID '$catalogId' is not $totalQty"

WebUI.executeJavaScript("alert('[Test Passed] Item $catalogId shows the correct total quantity: $totalQty')", null, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.dismissAlert()
