import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.trinoor.ElectronConnector

// don't require dataDirectoryPath and registrationUrl to be globally defined in the profile

'If dataDirectoryPath is neither passed in nor globally defined, set it to `null`'
if (dataDirectoryPath.isEmpty()) {
	if(GlobalVariable.metaClass.hasProperty(GlobalVariable, 'helix_dataDirectoryPath') != null) {
		// if it wasn't manually specified but globally defined, use the global definition
		dataDirectoryPath = GlobalVariable.helix_dataDirectoryPath
	} else {
		// if neither, set it to null
		dataDirectoryPath = null
	}
}


'If registrationUrl is neither passed in nor globally defined, set it to `null`'
if (registrationUrl.isEmpty()) {
	println "REGISTRATION URL IS EMPTY"
	if(GlobalVariable.metaClass.hasProperty(GlobalVariable, 'helix_registrationUrl') != null) {
		// if it wasn't manually specified but globally defined, use the global definition
		registrationUrl = GlobalVariable.helix_registrationUrl
	} else {
		// if neither, set it to null
		registrationUrl = null
	}
}

'Log the launch configuration'
KeywordUtil.logInfo('Launching TAShelix with configuration:')
KeywordUtil.logInfo(" - clientExecutablePath: $clientExecutablePath")
KeywordUtil.logInfo(" - chromeDriverPath:     $chromeDriverPath")
KeywordUtil.logInfo(" - dataDirectoryPath:    $dataDirectoryPath")
KeywordUtil.logInfo(" - loginCustomerCode:    $loginCustomerCode")
KeywordUtil.logInfo(" - loginUsername:        $loginUsername")
KeywordUtil.logInfo(" - registrationUrl:      $registrationUrl")

// Setup the electron chromedriver and launch the TAShelix client
ElectronConnector electronConnector = CustomKeywords.'com.trinoor.Electron.launchAndConnect'(
	clientExecutablePath,
	chromeDriverPath,
	dataDirectoryPath)

// Make sure the tashelix client didn't fail to launch
assert electronConnector.process.isAlive() : "TAShelix client failed to launch. Try making sure another instance is not using the same data directory: '$dataDirectoryPath'"

// Change Katalon's default driver to the one connected to our client
DriverFactory.changeWebDriver(electronConnector.driver)

'Wait for page to be ready'
WebUI.waitForElementPresent(findTestObject('Common/po_InnerText_Contains', [('innerText') : 'TAShelix']), 0)

'If registrationUrl is passed in or globally defined, modify the registration url'
boolean shouldModifyRegistrationUrl = false
if (registrationUrl) {
	shouldModifyRegistrationUrl = true
	KeywordUtil.logInfo("Using custom registration URL: '$registrationUrl'")

	// If we're using a custom dataDirectoryPath, check the registration-url file in that directory
	if (dataDirectoryPath) {
		// Look for the registration-url.json file to check the current registration url
		String registrationFilepath = "${dataDirectoryPath}/helix_components/registration-url.json"
		KeywordUtil.logInfo(" - checking if registration URL is present in '$dataDirectoryPath'")
		File registrationFile = new File(registrationFilepath)
		if (registrationFile.exists() && registrationFile.canRead()) {
			try {
				String registrationJson = registrationFile.text
				
				// If the current registration url matches this one, then don't modify it
				if (registrationJson ==~ /.*"registration_url":"$registrationUrl".*/) {
					KeywordUtil.logInfo(" - profile already using '$registrationUrl' - not modifying")
					shouldModifyRegistrationUrl = false
				}
			} catch (Exception e) {
			}
		}
	}
	if (shouldModifyRegistrationUrl) {
		WebUI.callTestCase(findTestCase('Common/ptc_EditRegistrationUrl'), [('registrationUrl') : registrationUrl], FailureHandling.STOP_ON_FAILURE)
	}
}

// Determine if we need to register or not by checking to see if the password field is visible
if (WebUI.verifyElementNotVisible(findTestObject('Login_page/input_Password'), FailureHandling.OPTIONAL)) {
	WebUI.callTestCase(
		findTestCase('Common/RegisterNewUser'),
		[
			loginCustomerCode: loginCustomerCode,
			loginUsername: loginUsername
		], FailureHandling.STOP_ON_FAILURE)
} else {
	KeywordUtil.logInfo('Skipping registration')
}

// Login
'Wait for the password dialog to appear'
WebUI.waitForElementVisible(findTestObject('Login_page/input_Password'), 10)

'Enter the password'
println "Setting password using $loginPassword"
WebUI.setEncryptedText(findTestObject('Login_page/input_Password'), loginPassword)

'Click the Login button'
WebUI.click(findTestObject('Login_page/button_Login'))

'If the offline pin dialog popped up, then enter a pin and click continue'
if (WebUI.verifyElementVisible(findTestObject('Login_page/input_OfflinePin'), FailureHandling.OPTIONAL)) {
	'Enter a pin'
	WebUI.setText(findTestObject('Login_page/input_OfflinePin'), '0000', FailureHandling.OPTIONAL)
	
	'Click the Continue button'
	WebUI.click(findTestObject('Login_page/button_Continue'))
}

'Wait for the app updater to appear'
WebUI.waitForElementVisible(findTestObject('Common/window_AppUpdater'), 3)

'Wait for the app updater to disappear'
WebUI.waitForElementNotPresent(findTestObject('Common/window_AppUpdater'), 0, FailureHandling.OPTIONAL)

'Wait for the Asset Suite module to appear'
WebUI.waitForElementPresent(findTestObject('Home_page/module_AssetSuite'), 300)
