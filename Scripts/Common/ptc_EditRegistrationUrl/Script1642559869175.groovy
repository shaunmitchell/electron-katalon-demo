import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import java.awt.event.KeyEvent
import java.awt.Robot

/**
 *  TODO:
 *   - [ ] modify the registration URL (optionally?) via modifying the
 *         ${dataDirectoryPath}/helix_components/registration-url.json file
 */

'Create a robot to send application-level keyboard shortcuts'
Robot robot = new Robot()

'Keys to press to bring up the url editor'
keys = [KeyEvent.VK_CONTROL, KeyEvent.VK_SHIFT, KeyEvent.VK_R]

'Press the keys'
keys.each{key -> robot.keyPress(key)}

'Release the keys'
keys.each{key -> robot.keyRelease(key)}

'Wait for the registration popup to appear'
WebUI.waitForElementVisible(findTestObject('Registration_popup/button_Edit'), 5)

'Click the edit button'
WebUI.click(findTestObject('Registration_popup/button_Edit'))

'Set the registration URL'
WebUI.setText(findTestObject('Registration_popup/input_Url'), registrationUrl)

'Click the Save button'
WebUI.click(findTestObject('Registration_popup/button_Save'))

'Close the dialog'
WebUI.click(findTestObject('Registration_popup/button_Close'))
