import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

KeywordUtil.logInfo("Registering user with:")
KeywordUtil.logInfo("  - Customer Code  '$loginCustomerCode'")
KeywordUtil.logInfo("  - Username       '$loginUsername'")

'Enter login credentials'
WebUI.setText(findTestObject('Login_page/input_CustomerCode'), loginCustomerCode)

WebUI.setText(findTestObject('Login_page/input_Username'), loginUsername)

WebUI.click(findTestObject('Login_page/button_Register'))

'Wait for registration timer to pop up'
WebUI.waitForElementPresent(findTestObject('Registration_popup/dialog_RegistrationTimer'), 10)

'Wait for registration timer to disappear'
WebUI.waitForElementNotPresent(findTestObject('Registration_popup/dialog_RegistrationTimer'), 10)

'Click the User Login button for the ABB user'
WebUI.click(findTestObject('Login_page/button_UserLogin'))
