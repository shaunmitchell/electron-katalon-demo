# #katalon-electron integration

## overview
trying to launch electron through a chromedriver using the standard
`.setBinary("/path/to/tashelix")` approach results in several weird issues:
network communication errors, long page load times, etc. instead, we will:

* launch the tashelix client separately adding the
  `--remote-debugging-port=1234` option
* create a chromedriver instance which connects to that port
* use `DriverFactory.changeWebDriver()` to changes katalon's default
  chromedriver to this custom instance

the last step allows us to use all of katalon's features as normal, e.g.:
creating test objects and `WebUI.click(...)`'ing on them.

if you just want to see the code, skip down to
[wrapping it up](#markdown-header-wrapping-it-up)

## setting it up

### determine the electron version
first, determine what version of chrome the electron app is running on. i
honestly haven't found a great way to do this. i had to try and launch tashelix
with a random chromedriver, and then in the error message saying there's a
version mismatch, it should tell you the version of chrome used by the electron
app. you can do this by running:

```java
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.WebDriver;

// Set the path to chromedriver
System.setProperty("webdriver.chrome.driver", "/path/to/any/chromedriver");

// Tell it to launch the tashelix client
ChromeOptions options = new ChromeOptions();
options.setBinary("/path/to/tashelix");

// Launch the driver
WebDriver driver = new ChromeDriver(options);
```

once this runs, you should see an error message similar to:

```
org.openqa.selenium.SessionNotCreatedException: session not created: This version of ChromeDriver only supports Chrome version 98
Current browser version is 94.0.4606.81 with binary path C:\Program Files\tashelix\tashelix.exe
```

in this case, `94.0.4606.81` is the chrome version our tashelix client is using,
and we'll use that to find a matching chromedriver

### download chromedriver
once you've found which version of chrome the tashelix client is using, you can
get the chromedriver binary you need either through the npm
[electron-chromedriver](https://www.npmjs.com/package/electron-chromedriver)
package or from the
[electron releases](https://github.com/electron/releases#releases) github
repository.

#### via npm
use the instructions on the `electron-chromedriver` homepage linked above. once
complete, copy the downloaded chromedriver binary from your
`node_modules/electron-chromedriver/bin/` folder to wherever makes your heart
happy.

#### via github
on the electron releases github page, scroll down until you find a matching
release. note: it's only necessary for the _major_ version to be a match. i.e.
for the 94.0.4606.81 electron client, i could use any 94.x.x.x electron release.
also note: do _not_ use the nightly release builds, because those only include
source code rather than compiled chromedriver binaries. look for a release in the
form `vX.Y.Z` e.g. `v15.0.0`

click on the release link in the leftmost cell, scroll down to the _Assets_
section, and download the version of chromedriver for your platform

### launch tashelix
we want to launch tashelix on its own–not through chromedriver–with the
`--remote-debugging-port` flag. for testing purposes, this can be as simple as
going to the command line and running:

```sh
$ /path/to/tashelix --remote-debugging-port=9222
```

for the purpose of automation, you'll probably want to use something like:

```
ProcessBuilder pb = new ProcessBuilder();
pb.command(['/path/to/tashelix', '--remote-debugging-port=9222'])
Process process = pb.start()
```

### connect through katalon
lastly we want to create a chromedriver instance that connects to this port:

```java
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.WebDriver

// Set the path to the chromedriver
System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver")

// Set the chromedriver to connect to the tashelix client debug port
ChromeOptions options = new ChromeOptions();
options.setExperimentalOption("debuggerAddress", "localhost:9222");

// Create a ChromeDriver instance
WebDriver driver = new ChromeDriver(options);

// Change Katalon's default chromedriver to this one
DriverFactory.changeWebDriver(driver)

// Verify it worked and that we can interact with TAShelix using Katalon methods
WebUI.delay(5)
KeywordUtil.logInfo("Electron client launched: " + WebUI.getTitle())
```

### wrapping it up
altogether, the core of the logic behind launching and connecting to tashelix is
thus:

```groovy
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.WebDriver

// Launch the tashelix client
ProcessBuilder pb = new ProcessBuilder();
pb.command(['/path/to/tashelix', '--remote-debugging-port=9222'])
Process process = pb.start()

// Set the path to the chromedriver
System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver")

// Set the chromedriver to connect to the tashelix client debug port
ChromeOptions options = new ChromeOptions();
options.setExperimentalOption("debuggerAddress", "localhost:9222");

// Create chromedriver
WebDriver driver = new ChromeDriver(options);

// Change Katalon's default chromedriver to this one
DriverFactory.changeWebDriver(driver)

// Verify it worked and that we can use Katalon methods inside TAShelix
WebUI.delay(5)
KeywordUtil.logInfo("Electron client launched: " + WebUI.getTitle())
```

## modularization
included in the repo is a Katalon CustomKeyword
[com.trinoor.Electron](Keywords/com/trinoor/Electron.groovy) with several
functions and classes to easily launch an electron application and/or connect to
a running electron instance. they're documented decently, and the
[`LaunchAndLogin`](Scripts/Common/LaunchAndLogin/Script1642698052238.groovy)
script demonstrates how to use them.

that said, since `LaunchAndLogin` handles launching and connecting to tashelix,
you should never need to touch the Electron CustomKeyword directly. the
[AS9-CUS-IM-0069](Scripts/AS9-CUS-IM-0069/Script1643389144880.groovy) script
demonstrates how to call the `LaunchAndLogin` script at the beginning of a test
case to log in, which should be all you need when writing test cases at this
point

### using `LaunchAndLogin` with custom settings
the `LaunchAndLogin` script is typically called like so:

```groovy
'Sign in to the TAShelix client'
WebUI.callTestCase(findTestCase('Common/LaunchAndLogin'), null, FailureHandling.STOP_ON_FAILURE)
```

this will launch it with whatever settings are globally defined in your profile.
if you want to launch it with specific settings in one particular test case,
you can overwrite any of the following variables:

* `chromeDriverPath`
* `clientExecutablePath`
* `dataDirectoryPath`
* `registrationUrl`
* `loginCustomerCode`
* `loginUsername`
* `loginPassword` (must be Katalon encrypted text)

like so:

```groovy
'Sign in to the TAShelix client'
WebUI.callTestCase(
  findTestCase('Common/LaunchAndLogin'),
  [
    chromeDriverPath: '/path/to/custom/chromedriver',
    clientExecutablePath: '/path/to/tashelix',
    dataDirectoryPath: '/path/to/custom/data/directory'
  ], FailureHandling.STOP_ON_FAILURE)
```

the `dataDirectoryPath` is used by the tashelix client for storing session
information. if it's not set, tashelix will use the tashelix settings folder
on on your system.

**note** at present, you must set the `dataDirectoryPath` to `null` if you
intend to pass in custom login credential variables. i've not implemented a
system for it to navigate the menu and login as a separate user when another
account exists since that is outside the scope of what we need.

## todo
this is still fairly incomplete. once i got it working, i had to shift my focus
to more tangible tasks. todo items:

- clean up the code (especially the ElectronProcess class)
- make the `LaunchAndLogin` script more robust
- modify `ptc_EditRegistrationUrl` to create or modify the
  `registration-url.json` file instead of using `Robot` to send the
  registration url keystroke
- investigate running electron headless (there's some way of creating a
  wrapper around electron to achieve this, but i've not yet looked into it much)
