<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>//button[contains(@class, 'p-button') and descendant::span[contains(@class, 'fa-search')]]</description>
   <name>button_Search</name>
   <tag></tag>
   <elementGuidId>8e8b80e7-e420-47db-8e66-8a3f515cf14a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(@class, 'p-button') and descendant::span[contains(@class, 'fa-search')]]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
