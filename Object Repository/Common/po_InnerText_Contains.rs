<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>A parameterized test object that searches for any element with a given innerText

Variables:
	innerText: the text to search for in an element</description>
   <name>po_InnerText_Contains</name>
   <tag></tag>
   <elementGuidId>c0894471-b66d-4904-9559-e30b5a7328a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(., '${innerText}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
