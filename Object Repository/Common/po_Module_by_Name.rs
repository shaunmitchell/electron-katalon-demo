<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Parameterized Test Object
Returns a module on the home page by name

Variables:
	name: the name of the module</description>
   <name>po_Module_by_Name</name>
   <tag></tag>
   <elementGuidId>8e548abc-bcb9-4593-8509-f3ee90bdec1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-card-title[normalize-space() = '${name}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
