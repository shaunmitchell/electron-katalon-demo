<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Parameterized Test Object
Returns an input element based on the specified label text

Variables:
    label: the label text to match</description>
   <name>po_Input_by_Label</name>
   <tag></tag>
   <elementGuidId>32f5dfe2-6603-477a-891b-c4ee196ed34e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@aria-describedby = concat(//label[text()='${label}']/@for, 'help')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
