<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Any popup modal covering the page</description>
   <name>Modal</name>
   <tag></tag>
   <elementGuidId>92433aa8-d9f1-4383-9d39-ff6d00f2461d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//region-container[@dock = 'dock_modal']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
