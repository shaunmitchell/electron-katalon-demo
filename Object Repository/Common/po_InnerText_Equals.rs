<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>A parameterized test object that searches for any element with the given exact innerText

Variables:
	innerText: the text to search for in an element</description>
   <name>po_InnerText_Equals</name>
   <tag></tag>
   <elementGuidId>a4ffcca3-9bb1-470b-b958-eae41db47413</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[normalize-space(.)='${innerText}']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
