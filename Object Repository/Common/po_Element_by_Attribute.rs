<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Parameterized Test Object
Selects an element given an attribute and value

Variables:
	attribute: the element attribute to look for
	value: the value of the selected attribute</description>
   <name>po_Element_by_Attribute</name>
   <tag></tag>
   <elementGuidId>c5748f61-7e12-4dd5-8d4c-773acfbd31e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@${attribute}='${value}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
