<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_CustomerCode</name>
   <tag></tag>
   <elementGuidId>9681a475-22d1-4e5e-b6e9-df370d728b3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[placeholder='Enter Customer Code *']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = 'Enter Customer Code *']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
