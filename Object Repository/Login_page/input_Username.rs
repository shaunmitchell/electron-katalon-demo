<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username</name>
   <tag></tag>
   <elementGuidId>c7b2c991-4ac7-4465-9771-1fc1f85d41d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[placeholder='Enter Customer Code *']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = 'Enter Username *']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
