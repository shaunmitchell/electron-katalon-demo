<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password</name>
   <tag></tag>
   <elementGuidId>e2163d0d-6419-4a98-988f-3c49787a6242</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[placeholder='Enter Customer Code *']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = 'Enter Password *']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
