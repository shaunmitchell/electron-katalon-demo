<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_OfflinePin</name>
   <tag></tag>
   <elementGuidId>6ae7ff9a-ebc7-4514-b971-8ccb5c151e01</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[placeholder='Enter Customer Code *']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@aria-describedby = concat(//label[normalize-space(.) = 'Offline Pin']/@for, 'help')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
