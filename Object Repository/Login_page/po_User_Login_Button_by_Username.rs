<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Parameterized Test Object
Returns the User Login button for a given username on the homepage</description>
   <name>po_User_Login_Button_by_Username</name>
   <tag></tag>
   <elementGuidId>9643e9f6-5728-429c-b84f-3ba76eb620d3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
