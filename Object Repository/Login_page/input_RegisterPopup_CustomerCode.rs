<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>References the Customer Code input box in the Registration popup</description>
   <name>input_RegisterPopup_CustomerCode</name>
   <tag></tag>
   <elementGuidId>a4087ddf-4224-4c2c-a30f-8d3226d5dbcd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//region-normal[descendant::div[@id='register']]//label[contains(normalize-space(), 'Customer Code')]/@for</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
