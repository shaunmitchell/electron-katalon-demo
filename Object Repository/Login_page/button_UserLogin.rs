<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>The first User Login button on the page</description>
   <name>button_UserLogin</name>
   <tag></tag>
   <elementGuidId>6cde19b2-225c-458f-b910-392de9b334d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[normalize-space(.) = 'User Login']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
