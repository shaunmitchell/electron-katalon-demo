<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close</name>
   <tag></tag>
   <elementGuidId>5d95ecaf-52de-4fcc-ac45-7b3e33828719</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-toolbar[normalize-space() = 'TAShelix Registration URL']/ancestor::mat-dialog-container//button[normalize-space(text()) = 'Close']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
