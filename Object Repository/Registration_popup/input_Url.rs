<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Url</name>
   <tag></tag>
   <elementGuidId>9dff6f6c-4b9d-4f5c-b09a-856756d98462</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-toolbar[normalize-space() = 'TAShelix Registration URL']/ancestor::mat-dialog-container//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
