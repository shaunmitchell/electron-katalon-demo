<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>The loading bar that pops up after you click &quot;Register&quot; for a new user</description>
   <name>dialog_RegistrationTimer</name>
   <tag></tag>
   <elementGuidId>84dfca34-2a56-4737-85c4-92063da02c67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//registration-timer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
