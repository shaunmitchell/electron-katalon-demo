<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Edit</name>
   <tag></tag>
   <elementGuidId>6b2c5208-dd0a-4186-b94a-22c8881aad34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-toolbar[normalize-space() = 'TAShelix Registration URL']/ancestor::mat-dialog-container//button[@icon = 'pi pi-pencil']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
